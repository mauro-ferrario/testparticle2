#version 120

#extension GL_ARB_draw_buffers : enable

// Hmm, do we really need to give the path to the shader if it's in the same folder?
#pragma include "Shaders/Common/ShaderHelpers.glslinc"
#pragma include "Shaders/Common/SimplexNoiseDerivatives4D.glslinc"
//#pragma include "Shaders/Common/Noise4D.glslinc"

uniform sampler2D positions;
uniform sampler2D startPosTexture;
uniform sampler2D mapOffset;
uniform sampler2D distortion;

uniform float u_time;
uniform float u_timeStep;
uniform float offsetHeight;

uniform float distortFriction;

uniform int shaderMode;


const int OCTAVES = 3;

uniform float u_noisePositionScale = 21.5; // some start values in case we don't set any
uniform float u_noiseMagnitude = 0.0175;
uniform float u_noiseTimeScale = 0.1; //1.0 / 4000.0;
uniform float u_noisePersistence = 0.92;
uniform vec3 u_baseSpeed = vec3( 0.0, 0.97, 0.0 );
uniform float u_particleMaxAge;




// -----------------------------------------------------------
void main (void)
{
	vec2 texCoord = gl_TexCoord[0].st;
  vec4 actualPos = texture2D( positions, texCoord );
	vec3 startPos  = texture2D( startPosTexture,texCoord ).xyz;
  vec4 offsetColor = texture2D( mapOffset, texCoord );
  vec4 distortionOffset = texture2D( distortion, texCoord );
  vec3 pos = startPos;
  
  
  float age = actualPos.w;
  
  
  
 /* */
  
  vec3 noisePosition = pos  * u_noisePositionScale;
  float noiseTime    = u_time * u_noiseTimeScale;
  
  vec4 xNoisePotentialDerivatives = vec4(0.0);
  vec4 yNoisePotentialDerivatives = vec4(0.0);
  vec4 zNoisePotentialDerivatives = vec4(0.0);
  
  float tmpPersistence = u_noisePersistence;
  
  for (int i = 0; i < OCTAVES; ++i)
  {
    float scale = (1.0 / 2.0) * pow(2.0, float(i));
    
    float noiseScale = pow(tmpPersistence, float(i));
    if (tmpPersistence == 0.0 && i == 0) //fix undefined behaviour
    {
      noiseScale = 1.0;
    }
    
    xNoisePotentialDerivatives += simplexNoiseDerivatives(vec4(noisePosition * pow(2.0, float(i)), noiseTime)) * noiseScale * scale;
    yNoisePotentialDerivatives += simplexNoiseDerivatives(vec4((noisePosition + vec3(123.4, 129845.6, -1239.1)) * pow(2.0, float(i)), noiseTime)) * noiseScale * scale;
    zNoisePotentialDerivatives += simplexNoiseDerivatives(vec4((noisePosition + vec3(-9519.0, 9051.0, -123.0))  * pow(2.0, float(i)), noiseTime)) * noiseScale * scale;
  }
  
  //compute curl
  vec3 noiseVelocity = vec3( zNoisePotentialDerivatives[1] - yNoisePotentialDerivatives[2],
                            xNoisePotentialDerivatives[2] - zNoisePotentialDerivatives[0],
                            yNoisePotentialDerivatives[0] - xNoisePotentialDerivatives[1] ) * u_noiseMagnitude;
  
  vec3 totalVelocity = u_baseSpeed + noiseVelocity;
  
  
  /**/
  
  if(shaderMode == 0){
   // pos.y += offsetColor.y * offsetHeight;
    if(actualPos.y == 0){
      actualPos.y = 0.1;
    }
    pos = actualPos.xyz;
    vec3 newPos = pos + totalVelocity * u_timeStep;
    vec3 vel = newPos - pos;
    pos = newPos;
    //pos += u_baseSpeed*0.01;;
    //pos = vec3(startPos.x, 0.0, startPos.z);
    age += u_timeStep * 10.0; // * noiseVelocity.y * 1000.0;
    if(age == 0.0){
      age = 0.01;
    }
    if(age > u_particleMaxAge){
      age = 0.001; //(noiseVelocity.x * 100.0)+0.1;
      pos = vec3(startPos.x, 0.0, startPos.z);
    }
    
    /*
    if(pos.y <= 0.30){
      pos.y = 0.3;
      age = 0.0;
    }
  */
    /*
     if( age > 0.5 )
     {
     age = 0.0;
     
     float spawnRadius = 0.1;
     pos =startPos
     }
     */
    
  }
  if(shaderMode == 1){
    float upForce = distortionOffset.y * offsetHeight;
    float currentY = pos.y;
    float targetY = pos.y + distortionOffset.y * offsetHeight;
    float nextPosY = currentY;
    if(targetY > currentY)
      nextPosY += upForce*distortFriction;
    pos.y = nextPosY;
    pos.y = startPos.y + distortionOffset.x * offsetHeight;
  }
   
	gl_FragData[0] = vec4(pos ,age );
}

