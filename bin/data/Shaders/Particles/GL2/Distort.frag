#version 120

#extension GL_ARB_draw_buffers : enable

// Hmm, do we really need to give the path to the shader if it's in the same folder?
#pragma include "Shaders/Common/ShaderHelpers.glslinc"
//#pragma include "Shaders/Common/SimplexNoiseDerivatives4D.glslinc"
#pragma include "Shaders/Common/Noise4D.glslinc"

// uniform sampler2D positions;
uniform sampler2D distort;
uniform sampler2D newPoints;

uniform float u_time;
uniform float u_timeStep;
uniform float offsetHeight;

uniform float distortFriction;


uniform float upForce;
uniform float downForce;

// -----------------------------------------------------------
void main (void)
{
  vec2 texCoord = gl_TexCoord[0].st;
  // vec4 actualPos = texture2D( positions, texCoord );
  vec3 distortPos  = texture2D( distort,texCoord ).xyz;
  vec4 newPointsPos = texture2D( newPoints, texCoord );
  vec3 pos = distortPos.xyz;
  
  if(newPointsPos.x > pos.x){
    // Aumenta
    pos.x += newPointsPos.x * upForce;
  }
  else{
    pos.x -= downForce * 0.01;
  }
  if(pos.x < 0.0){
    pos.x = 0.0;
  }
  pos.y = 0.0;
  pos.z = 0.0;
  
  /*
  vec4 distortionOffset = texture2D( distortion, texCoord );
  pos.y += offsetColor.y * offsetHeight;
  
  float upForce = distortionOffset.y * offsetHeight;
  float currentY = pos.y;
  float targetY = pos.y + distortionOffset.y * offsetHeight;
  float nextPosY = currentY;
  if(targetY > currentY)
    //nextPosY += (targetY - currentY) * distortFriction;
    nextPosY += upForce*distortFriction;
  pos.y = nextPosY;
  */
  
  
  gl_FragData[0] = vec4(pos , 1.0 );
}

