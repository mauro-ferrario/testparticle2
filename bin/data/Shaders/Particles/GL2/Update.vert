#version 120

varying vec4 offsetV;
uniform sampler2D mapOffset;
// -----------------------------------------------------------
void main()
{
  gl_TexCoord[0] = gl_MultiTexCoord0;
  vec2 texCoord = gl_TexCoord[0].st;
  gl_FrontColor = gl_Color;
  gl_TexCoord[0] = gl_MultiTexCoord0;
  vec4 offsetColor = texture2D( mapOffset, texCoord );
  offsetV = offsetColor;
  gl_Position = ftransform();
}

