#version 120

uniform sampler2D u_particleImageTexture;
varying vec4 v_particleColor;
uniform float particleAlpha;


varying vec3 particlePos;

void main ()
{
  vec2 texCoord = gl_TexCoord[0].st;
	vec4 particleSpriteTexCol = texture2D( u_particleImageTexture, gl_PointCoord.xy );
 // vec4 particleColor = texture2D( u_particleColorsTexture, texCoord );
	//vec4 color =  particleSpriteTexCol * v_particleColor;
  vec4 color =   v_particleColor; // * particleSpriteTexCol;
  //if (color.a <= 0.0) discard;
  float alpha = particleAlpha * particlePos.y;
  //alpha = 1.0;
  
	gl_FragColor = color;
  gl_FragColor = vec4(texCoord.y*texCoord.x,texCoord.y,texCoord.x, alpha) * particleSpriteTexCol;
 // gl_FragColor = vec4(vec3(particleAlpha),1.0);
}


