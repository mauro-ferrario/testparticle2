#version 120

#pragma include "Shaders/Common/ShaderHelpers.glslinc"

uniform sampler2D u_particleDataTexture;
uniform sampler2D mapOffset;
uniform mat4 u_viewMatrix;
uniform mat4 u_projectionMatrix;
uniform mat4 u_modelViewProjectionMatrix;
uniform float u_particleDiameter;
uniform float u_screenWidth;

uniform float particleDistance;
varying vec4 v_particleColor;
varying vec3 particlePos;

uniform float rotation;
uniform sampler2D distortion;


// ----------------------------
void main ()
{
	gl_TexCoord[0] = gl_MultiTexCoord0;
	vec2 texCoord = gl_TexCoord[0].st;
	vec4 particleData = texture2D( u_particleDataTexture, texCoord );
  vec4 distortionOffset = texture2D( distortion, particleData.xz );
  vec4 offsetColor = texture2D( mapOffset, texCoord );
	vec3 pos = particleData.xyz * particleDistance;
  particlePos = pos.xyz;
  v_particleColor = offsetColor; // vec4(1.0,1.0,1.0,1.0);
	vec3 viewSpacePosition = vec3(u_viewMatrix * vec4(pos, 1.0));
	vec4 corner = vec4(u_particleDiameter * 0.5, u_particleDiameter * 0.5, viewSpacePosition.z, 1.0);
	float projectedCornerX = dot(vec4(u_projectionMatrix[0][0], u_projectionMatrix[1][0], u_projectionMatrix[2][0], u_projectionMatrix[3][0]), corner);
	float projectedCornerW = dot(vec4(u_projectionMatrix[0][3], u_projectionMatrix[1][3], u_projectionMatrix[2][3], u_projectionMatrix[3][3]), corner);
  gl_PointSize = pos.y * u_particleDiameter; //u_screenWidth * 0.5 * projectedCornerX * 2.0 / projectedCornerW;
  float pointSize =u_particleDiameter * distortionOffset.x * 10.0;
  if(pointSize <= 0.0){
    pointSize = 0.1;
  }
  gl_PointSize = pointSize;
  
  //mat4 rotationMatrix(vec3 axis, float angle)
  
	gl_Position = u_modelViewProjectionMatrix * rotationMatrix(vec3(0.0,1.0,0.0), rotation) * vec4(pos, 1.0);
}
