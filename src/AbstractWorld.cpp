//
//  AbstractWorld.cpp
//  testParticle2
//
//  Created by xxx on 22/04/2018.
//

#include "AbstractWorld.h"



void AbstractWorld::setup(){
  this->setupVariables();
  this->initParticlesEmitter();
  this->initCamera();
  this->initGUI();
}

void AbstractWorld::update(){
  if( ofGetKeyPressed(' ') ) { timeStep = ofLerp( timeStep, ofMap( ofGetMouseX(), 0, ofGetWidth(), -(1.0f/60.0f), (1.0f/60.0f) ), 0.1f );}
  else { timeStep = ofLerp( timeStep, 1.0f / 120.0f, 0.1f ); } //  *********************** TEMP, slowing down time a bit, set back to normal time once we change the sim
  // Update time, this let's us hit space and slow down time, even reverse it.
  time += timeStep;
  particles.update( time, timeStep );
}

void AbstractWorld::initGUI(){
  gui = new ofxDatGui( ofxDatGuiAnchor::TOP_RIGHT );
  gui->addHeader(":: XXX ::");
  ofxDatGuiButton* toggleGrid = gui->addToggle("Toggle grid", bDrawGrid);
  gui->onToggleEvent(this, &AbstractWorld::onToggleEvent);
  gui->addSlider("Camera speed", 0.00f, 1.00f);
  gui->addSlider("Time step", -0.20f, 0.20f);
  gui->getSlider("Time step")->setPrecision(4);
  gui->onSliderEvent(this, &AbstractWorld::onSliderChange);
  gui->addFolder(particles.getGUI());
  this->gui->setVisible(bDrawGui);
}

void AbstractWorld::toggleGUI(){
  bDrawGui = !bDrawGui;
}

void AbstractWorld::draw(){
  ofBackground(0, 0, 0);
  this->drawScene();
  ofDisableDepthTest();
  ofEnableBlendMode( OF_BLENDMODE_ALPHA );
  ofSetColor( ofColor::white );
  ofDrawBitmapString(ofToString(ofGetFrameRate(),2), ofGetWidth()-35, ofGetHeight() - 6);
}

void AbstractWorld::drawScene(){
  camera.begin();
  if(bDrawGrid){
    this->drawGrid();
  }
  ofSetColor( ofColor::white );
  particles.draw( &camera );
  camera.end();
}

void AbstractWorld::drawGrid(){
  ofSetColor( ofColor(60) );
  ofPushMatrix();
  ofRotate(90, 0, 0, -1);
  ofDrawGridPlane( 0.5, 12, false ); // of 0.9.0
  ofPopMatrix();
}

void AbstractWorld::onSliderChange(ofxDatGuiSliderEvent e){
  if(e.target->getName() == "Camera speed"){
    camera.setMovementMaxSpeed( e.value);
  }
  if(e.target->getName() == "Time step"){
    timeStep = e.value;
  }
}

void AbstractWorld::setupVariables(){
  bDrawGui = false;
  bDrawGrid = true;
  texSize = 1024;
}

void AbstractWorld::drawGUI(){
  ofPushMatrix();
  ofScale(0.25, 0.25);
  particles.distortFbo.draw(0,0);
  particles.particlePosFbo.source()->draw(0,1024);
  particles.particleDistortFbo.source()->draw(1024,0);
  ofPopMatrix();
}

void AbstractWorld::onToggleEvent(ofxDatGuiToggleEvent e){
  if(e.target->getName() == "Toggle grid"){
    bDrawGrid = e.target->getChecked();
  }
}

void AbstractWorld::initCamera(){
  camera.setNearClip(0.01f);
  camera.setPosition( 0, 0.5, 0.9 );
  camera.setMovementMaxSpeed( 0.1f );
}

void AbstractWorld::initParticlesEmitter(){
  particles.init(texSize);
}
