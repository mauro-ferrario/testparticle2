//
//  ParticleSystem.cpp
//  testParticle2
//
//  Created by xxx on 11/03/2018.
//

#include "ParticleSystem.h"


ParticleSystem::ParticleSystem(){
  
}

void ParticleSystem::init(int _texSize){
  ofDisableArbTex();
  map.load( "map.jpg" );
  particleImage.load( "Textures/Soft64.png" );
  ofEnableArbTex();
  startPositions.setFromImage("map2.jpg");
  maxAge = 10.0;
  this->initGUI();
  this->initVariables(_texSize);
  this->initFboSettings();
  this->initPosFbo();
  this->initDistortForceFbo();
  this->setupStartPositionsFromTextureShape(startPositions.startPositions, startPositions.startColors);
//  this->setupStartPositions();
  this->setupDistortForce();
  this->initShaders();
  this->initParticleMesh();
  rotation = 0;
  
  
  distortCircle.load("distortCircle.png");
  distortFbo.allocate(fboSettings);
  distortFbo.begin();
  ofPushStyle();
  ofClear(0,0,0,1);
//  ofSetColor(255,0,0);
//  ofDrawRectangle(0, 0, 1024*0.5, 1024);
//  ofSetColor(0,0,0);
//  ofDrawRectangle(1024*0.5,0, 1024*0.5,  1024);
  ofPopStyle();
  distortFbo.end();
}


void ParticleSystem::setupDistortForce(){
  ofFloatPixels startPosBuffer;
  ofVec4f* startPositionsAndAge = new ofVec4f[numParticles];
  startPosBuffer.allocate( fboSettings.width, fboSettings.height, 4 );
  int tmpIndex = 0;
  for( int y = 0; y < textureSize; y++ )
  {
    for( int x = 0; x < textureSize; x++ )
    {
      startPosBuffer.getPixels()[ (tmpIndex * 4) + 0 ] = 0.0;
      startPosBuffer.getPixels()[ (tmpIndex * 4) + 1 ] = 0.0;
      startPosBuffer.getPixels()[ (tmpIndex * 4) + 2 ] = 0.0;
      startPosBuffer.getPixels()[ (tmpIndex * 4) + 3 ] = 1.0;
      tmpIndex++;
    }
  }
  particleDistortFbo.source()->getTextureReference(0).loadData(startPosBuffer);
}

void ParticleSystem::setupStartPositions(){
  ofFloatPixels startPosBuffer;
  ofVec4f* startPositionsAndAge = new ofVec4f[numParticles];
  startPosBuffer.allocate( fboSettings.width, fboSettings.height, 4 );
  int tmpIndex = 0;
  for( int y = 0; y < textureSize; y++ )
  {
    for( int x = 0; x < textureSize; x++ )
    {
      ofVec3f spawnPos;
      //int randomPos =(int)ofRandom(startPositions.size()-1);
      spawnPos = ofVec3f(-(textureSize*0.5)*0.05+x*0.05, 0.0, -(textureSize*0.5)*0.05+y*+0.05); //startPositions.at( randomPos );
      startPositionsAndAge[tmpIndex] = ofVec4f( spawnPos.x, spawnPos.y, spawnPos.z, 0.0 );
      startPosBuffer.getPixels()[ (tmpIndex * 4) + 0 ] = spawnPos.x;
      startPosBuffer.getPixels()[ (tmpIndex * 4) + 1 ] = spawnPos.y;
      startPosBuffer.getPixels()[ (tmpIndex * 4) + 2 ] = spawnPos.z;
      startPosBuffer.getPixels()[ (tmpIndex * 4) + 3 ] = 0.0;
      tmpIndex++;
    }
  }
  particlePosFbo.source()->getTextureReference(0).loadData(startPosBuffer);
  ofDisableTextureEdgeHack();
  startPosTexture.allocate( startPosBuffer, false );
  startPosTexture.setTextureMinMagFilter(GL_NEAREST, GL_NEAREST);
  startPosTexture.setTextureWrap( GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE );
  startPosTexture.loadData( startPosBuffer );
  ofEnableTextureEdgeHack();
}

void ParticleSystem::setupStartPositionsFromTextureShape(vector<ofVec3f> startPositions, vector<ofVec3f> startColors){
  ofFloatPixels startPosBuffer;
  ofFloatPixels startColorsBuffer;
  ofVec4f* startPositionsAndAge = new ofVec4f[numParticles];
  startPosBuffer.allocate( fboSettings.width, fboSettings.height, 4 );
  int tmpIndex = 0;
  for( int y = 0; y < textureSize; y++ )
  {
    for( int x = 0; x < textureSize; x++ )
    {
      ofVec3f spawnPos;
      int randomPos =(int)ofRandom(startPositions.size()-1);
      spawnPos = startPositions.at( randomPos );
      float startAge = ofRandom( maxAge );
      startPositionsAndAge[tmpIndex] = ofVec4f( spawnPos.x, spawnPos.y, spawnPos.z, startAge );
     startPosBuffer.getPixels()[ (tmpIndex * 4) + 0 ] = spawnPos.x;
      startPosBuffer.getPixels()[ (tmpIndex * 4) + 1 ] = spawnPos.y;
      startPosBuffer.getPixels()[ (tmpIndex * 4) + 2 ] = spawnPos.z;
      startPosBuffer.getPixels()[ (tmpIndex * 4) + 3 ] = startAge;
      tmpIndex++;
    }
  }
  particlePosFbo.source()->getTextureReference(0).loadData(startPosBuffer);
  
  ofDisableTextureEdgeHack();
  startPosTexture.allocate( startPosBuffer, false );
  startPosTexture.setTextureMinMagFilter(GL_NEAREST, GL_NEAREST);
  startPosTexture.setTextureWrap( GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE );
  startPosTexture.loadData( startPosBuffer );
  ofEnableTextureEdgeHack();
}

void ParticleSystem::update(float _time, float _timeStep){
  rotation += speedRotation;
  ofEnableBlendMode( OF_BLENDMODE_ALPHA );
  distortFbo.begin();
 // ofClear(0,0,0,1);
  ofSetColor(0,cleanSpeed * 255);
  ofDrawRectangle(0, 0, 1024, 1024);
  for(int a = 0; a < 10; a++){
    ofSetColor(ofRandom(255));
    if(ofRandom(1) < .013){
      float size = ofRandom(100,500);
      distortCircle.draw(ofRandom(100,1000), ofRandom(0,1000),size, size);
      //ofDrawCircle(ofRandom(100,1000), ofRandom(0,1000), ofRandom(10,200));
    }
  }
  distortFbo.end();
  
  ofEnableBlendMode( OF_BLENDMODE_DISABLED ); // Important! We just want to write the data as is to the target fbo
  this->updateDistortion(_time, _timeStep);;
  particleDistortFbo.swap();
  this->updateParticlePositions(_time, _timeStep);
  particlePosFbo.swap();
}

void ParticleSystem::draw(ofCamera* _camera){
  ofSetColor( ofColor::white );
  ofEnableBlendMode( OF_BLENDMODE_ADD );
  //ofEnableBlendMode( OF_BLENDMODE_ALPHA );
  ofEnablePointSprites();
  particleDrawShader.begin();
  particleDrawShader.setUniform2f("u_resolution", particleImage.getWidth(), particleImage.getHeight() );
  particleDrawShader.setUniformTexture("u_particleImageTexture", particleImage.getTexture(), 0 );
  particleDrawShader.setUniformTexture("u_particleDataTexture", particlePosFbo.source()->getTexture(), 1 );
  particleDrawShader.setUniformTexture("mapOffset", map.getTexture(), 2 );
  
  particleDrawShader.setUniform2f("u_resolution", particleImage.getWidth(), particleImage.getHeight() );
  particleDrawShader.setUniformTexture("u_particleImageTexture", particleImage.getTextureReference(), 3);
  
  particleDrawShader.setUniformMatrix4f("u_viewMatrix", _camera->getModelViewMatrix() );
  particleDrawShader.setUniformMatrix4f("u_projectionMatrix", _camera->getProjectionMatrix() );
  particleDrawShader.setUniformMatrix4f("u_modelViewProjectionMatrix", _camera->getModelViewProjectionMatrix() );
  particleDrawShader.setUniform1f("u_particleDiameter", particleSize );
  particleDrawShader.setUniform1f("particleAlpha", particleOpacity );
  particleDrawShader.setUniform1f("particleDistance", particleDistance );
  particleDrawShader.setUniform1f("u_screenWidth", ofGetWidth() );
  particleDrawShader.setUniform1f("rotation", rotation);
  particleDrawShader.setUniformTexture("distortion", particleDistortFbo.source()->getTexture(), 2 );
  particlePoints.draw();
  particleDrawShader.end();
  ofDisablePointSprites();
}

void ParticleSystem::initGUI(){
  gui = new ofxDatGuiFolder("Particles");
  ofxDatGuiSlider* shaderModeSlider = gui->addSlider("Shader mode", 0, 1, 0);
  shaderModeSlider->bind(shaderMode);
  shaderModeSlider->setPrecision(1);
  ofxDatGuiSlider* sliderParticleOpacity = gui->addSlider("Particle Opacity", 0.0, 1.0, 0.1f);
  sliderParticleOpacity->bind(particleOpacity);
  sliderParticleOpacity->setPrecision(4, false);
  ofxDatGuiSlider* sliderOffsetHeight = gui->addSlider("Offset height", 0.01f, 10.00f, 5.0f);
  sliderOffsetHeight->bind(offsetHeight);
  sliderOffsetHeight->setPrecision(4, false);
  ofxDatGuiSlider* sliderParticleSize = gui->addSlider("Particle size", 0.01f, 10.00f, 0.01f);
  sliderParticleSize->bind(particleSize);
  sliderParticleSize->setPrecision(4, false);
  ofxDatGuiSlider* sliderParticleDistance = gui->addSlider("Particle distance", 0.01f, 10.00f, 0.05f);
  sliderParticleDistance->bind(particleDistance);
  ofxDatGuiSlider* sliderSpeedRotation = gui->addSlider("Rotation speed", -0.05f, 0.05f, 0.00f);
  sliderSpeedRotation->setPrecision(4, false);
  sliderSpeedRotation->bind(speedRotation);
  gui->addLabel("Particle mode 2 params");
  ofxDatGuiSlider* sliderCleanSpeed = gui->addSlider("Clean speed", 0.01f, 0.05f, 0.15f);
  sliderCleanSpeed->bind(cleanSpeed);
  ofxDatGuiSlider* sliderDistortionFriction = gui->addSlider("Distort Friction", 0.01f, 1.00f, 0.05f);
  sliderDistortionFriction->bind(distortFriction);
  ofxDatGuiSlider* sliderUpForce = gui->addSlider("Up force", 0.01f, 1.00f, 0.05f);
  sliderUpForce->bind(upForce);
  ofxDatGuiSlider* sliderDownForce = gui->addSlider("Down force", 0.01f, 1.00f, 0.05f);
  sliderDownForce->bind(downForce);
  gui->onSliderEvent(this, &ParticleSystem::onSliderChange);
  gui->addLabel("Particle noise");
  ofxDatGuiSlider* sliderAge = gui->addSlider("Particle Max Age", 0.0f, 120.0f, 10.0f);
  sliderAge->bind(maxAge);
  ofxDatGuiSlider* sliderNoiseMagnitude = gui->addSlider("Noise Magnitude",  0.01f, 1.0f, 0.075);
  sliderNoiseMagnitude->setPrecision(4);
  sliderNoiseMagnitude->bind(noiseMagnitude);
  ofxDatGuiSlider* sliderNoisePositionScale = gui->addSlider("Noise Position Scale", 0.01f, 5.0f, 1.5f);
  sliderNoisePositionScale->setPrecision(4);
  sliderNoisePositionScale->bind(noisePositionScale);
  ofxDatGuiSlider* sliderNoiseTimeScale = gui->addSlider("Noise Time Scale", 0.001f, 1.0f, 1.0 / 4000.0);
  sliderNoiseTimeScale->setPrecision(4);
  sliderNoiseTimeScale->bind(noiseTimeScale);
  ofxDatGuiSlider* sliderNoisePersistence = gui->addSlider("Noise Persistence", 0.001f, 1.0f, 0.2);
  sliderNoisePersistence->setPrecision(4);
  sliderNoisePersistence->bind(noisePersistence);
}

void ParticleSystem::onSliderChange(ofxDatGuiSliderEvent e){
}

ofxDatGuiFolder* ParticleSystem::getGUI()
{
  if(gui == NULL){
    this->initGUI();
  }
  return gui;
}

void ParticleSystem::initShaders(){
  particlePosShader.load("Shaders/Particles/GL2/Update");
  particleDrawShader.load("Shaders/Particles/GL2/DrawUnsorted");
  particleDistortShader.load("Shaders/Particles/GL2/Distort");
}

void ParticleSystem::initParticleMesh(){
  ofVec4f* startPositionsAndAge = new ofVec4f[numParticles];
  particlePoints.setMode( OF_PRIMITIVE_POINTS );
  for( int y = 0; y < textureSize; y++ )
  {
    for( int x = 0; x < textureSize; x++ )
    {
      ofVec2f texCoord;
      texCoord.x = ofMap( x + 0.5f,    0, textureSize,    0.0f, 1.0f ); // the original source has a  '+ 0.5' in it, to get the ceil?
      texCoord.y = ofMap( y + 0.5f,    0, textureSize,    0.0f, 1.0f );
      particlePoints.addVertex( ofVec3f(x,y,0) ); // this cuould be 0,0,0 as we won't use it, but put somehting in here so you can draw it without a shader to confirm it draws
      particlePoints.addTexCoord( texCoord );
    }
  }
}

void ParticleSystem::initVariables(int _texSize){
  textureSize = _texSize;
  numParticles = textureSize * textureSize;
  particleSize = 0.01;
  particleOpacity = 0.5;
  offsetHeight = 5.0f;
  particleSize = 0.01;
  particleDistance = 2.6303;
  cleanSpeed = 0.05;
  distortFriction = 0.02;
  upForce = 0.07;
  downForce = 0.16;
}

void ParticleSystem::initFboSettings(){
  fboSettings.width  = textureSize;
  fboSettings.height = textureSize;
  fboSettings.numColorbuffers = 1;
  fboSettings.useDepth = false;
  fboSettings.internalformat = GL_RGBA32F;    // Gotta store the data as floats, they won't be clamped to 0..1
  fboSettings.textureTarget = GL_TEXTURE_2D;
  fboSettings.wrapModeHorizontal = GL_CLAMP_TO_EDGE;
  fboSettings.wrapModeVertical = GL_CLAMP_TO_EDGE;
  fboSettings.minFilter = GL_NEAREST; // No interpolation, that would mess up data reads later!
  fboSettings.maxFilter = GL_NEAREST;
}

void ParticleSystem::updateDistortion(float _time, float _timeStep){
  particleDistortFbo.dest()->begin();
  particleDistortFbo.dest()->activateAllDrawBuffers(); // if we have multiple color buffers in our FBO we need this to activate all of them
  particleDistortShader.begin();
  particleDistortShader.setUniformTexture( "distort", particleDistortFbo.source()->getTexture(), 0 );
  particleDistortShader.setUniformTexture( "newPoints", distortFbo, 1 );
  particleDistortShader.setUniform1f("u_time", _time );
  particleDistortShader.setUniform1f("u_timeStep", _timeStep );
  particleDistortShader.setUniform1f("upForce", upForce );
  particleDistortShader.setUniform1f("downForce", downForce );
  particleDistortFbo.source()->draw(0,0);
  particleDistortShader.end();
  particleDistortFbo.dest()->end();
}

void ParticleSystem::updateParticlePositions(float _time, float _timeStep){
  particlePosFbo.dest()->begin();
  particlePosFbo.dest()->activateAllDrawBuffers(); // if we have multiple color buffers in our FBO we need this to activate all of them
  particlePosShader.begin();
  particlePosShader.setUniformTexture( "positions", particlePosFbo.source()->getTexture(), 0 );
  particlePosShader.setUniformTexture( "startPosTexture", startPosTexture, 1 );
  particlePosShader.setUniformTexture("mapOffset", map.getTexture(), 2 );
  particlePosShader.setUniformTexture("distortion", particleDistortFbo.source()->getTexture(), 3 );
  particlePosShader.setUniform1f("offsetHeight", offsetHeight );
  particlePosShader.setUniform1f("distortFriction", distortFriction );
  particlePosShader.setUniform1f("u_time", _time );
  particlePosShader.setUniform1f("u_timeStep", _timeStep );
  particlePosShader.setUniform1i("shaderMode", shaderMode );
  
  particlePosShader.setUniform1f("u_particleMaxAge", maxAge );
  particlePosShader.setUniform1f("u_noisePositionScale", noisePositionScale );
  particlePosShader.setUniform1f("u_noiseTimeScale", noiseTimeScale );
  particlePosShader.setUniform1f("u_noisePersistence", noisePersistence );
  particlePosShader.setUniform1f("u_noiseMagnitude", noiseMagnitude );
  
  
  particlePosFbo.source()->draw(0,0);
  particlePosShader.end();
  particlePosFbo.dest()->end();
}

void ParticleSystem::initPosFbo(){
  ofDisableTextureEdgeHack();
  particlePosFbo.allocate( fboSettings );
  ofEnableTextureEdgeHack();
}

void ParticleSystem::initDistortForceFbo(){
  ofDisableTextureEdgeHack();
  particleDistortFbo.allocate( fboSettings );
  ofEnableTextureEdgeHack();
}
