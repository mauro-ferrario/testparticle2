#include "ofApp.h"


//--------------------------------------------------------------
void ofApp::setup(){
  ofSetFrameRate( 60 );
  ofSetVerticalSync(false);
  abastractWorld.setup();
}


//--------------------------------------------------------------
void ofApp::update(){
  abastractWorld.update();
}

//--------------------------------------------------------------
void ofApp::draw(){
  abastractWorld.draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
  if(key == 'f'){
    ofToggleFullscreen();
  }
  if(key == 'g'){
    /*
    bDrawGui = !bDrawGui;
    this->gui->setVisible(bDrawGui);
     */
  }
}


