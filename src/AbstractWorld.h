//
//  AbstractWorld.hpp
//  testParticle2
//
//  Created by xxx on 22/04/2018.
//

#ifndef AbstractWorld_hpp
#define AbstractWorld_hpp

#include "ofMain.h"
#include "Utils/Cameras/ofxFirstPersonCamera.h"
#include "Utils/DrawingHelpers.h"
#include "ParticleSystem.h"
#include "ofxDatGui.h"

class AbstractWorld{
public:
  void                  setup();
  ofxDatGui*            gui;
  ParticleSystem        particles;
  ofxFirstPersonCamera  camera;
  void                  update();
  void                  draw();
  bool                  bDrawGui;
  int                   texSize;
  float                 time;
  float                 timeStep;
  void                  setupVariables();
  void                  initParticlesEmitter();
  void                  drawScene();
  void                  drawGrid();
  void                  initCamera();
  void                  initGUI();
  void                  onToggleEvent(ofxDatGuiToggleEvent e);
  void                  onSliderChange(ofxDatGuiSliderEvent e);
  void                  drawGUI();
  bool                  bDrawGrid;
  void                  toggleGUI();
};

#endif /* AbstractWorld_hpp */
