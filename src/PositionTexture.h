//
//  PositionTexture.hpp
//  testParticleEmitter
//
//  Created by xxx on 07/03/2018.
//

#ifndef PositionTexture_hpp
#define PositionTexture_hpp

#include "ofMain.h"

class PositionTexture{
public:
  PositionTexture();
  void                  setFromImage(string url);
  void                  updatePositions();
  ofImage               map;
  vector<ofVec3f>       startPositions;
  vector<ofVec3f>       startColors;
};

#endif /* PositionTexture_hpp */

