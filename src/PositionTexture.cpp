//
//  PositionTexture.cpp
//  testParticleEmitter
//
//  Created by xxx on 07/03/2018.
//

#include "PositionTexture.h"


PositionTexture::PositionTexture(){
  
}

void PositionTexture::setFromImage(string url){
  map.load(url);
  this->updatePositions();
}

void PositionTexture::updatePositions(){
  if( map.isAllocated()){
    startPositions.clear();
    startColors.clear();
    ofPixels& maskPixels = map.getPixelsRef();
    int channels = maskPixels.getNumChannels();
    int w = maskPixels.getWidth();
    int h = maskPixels.getHeight();
    float relwh = w/h;
    float relhw = h/w;
    int tmpIndex = 0;
    for( int y = 0; y < h; y++ )
    {
      for( int x = 0; x < w; x++ )
      {
        ofVec3f maskValue;
        maskValue.x = maskPixels[ (tmpIndex * channels) + 0 ];
        maskValue.y = maskPixels[ (tmpIndex * channels) + 1 ];
        maskValue.z = maskPixels[ (tmpIndex * channels) + 2 ];
        float maskSumValue =maskValue.x + maskValue.y + maskValue.z;
        if( (maskSumValue) > 0 )
        {
          ofVec3f startPos;
          float scale = 10;
          startPos.x = ofMap( x, 0, w, -.5*scale, .5*scale );
          startPos.y = maskValue.x/255; //ofMap( y, 0, h,  0.0, 0.75 );
          startPos.z = ofMap( y, 0, h,  -0.5*scale, 0.5*scale );
          startPositions.push_back( startPos );
          startColors.push_back( ofVec3f(maskValue.x/255.0,maskValue.y/255.0,maskValue.z/255.0));
        }
        tmpIndex++;
      }
    }
  }
}

