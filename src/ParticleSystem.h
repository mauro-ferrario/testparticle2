//
//  ParticleSystem.hpp
//  testParticle2
//
//  Created by xxx on 11/03/2018.
//

#ifndef ParticleSystem_hpp
#define ParticleSystem_hpp

#include "ofMain.h"
#include "ofxDatGui.h"
#include "ofxAutoReloadedShader.h"
#include "FboPingPong.h"
#include "Math/MathUtils.h"
#include "PositionTexture.h"


class ParticleSystem {
public:
  
                        ParticleSystem();
  void                  init(int _texSize);
  void                  update(float _time, float _timeStep);
  void                  draw(ofCamera* _camera);
  void                  initGUI();
  void                  initShaders();
  void                  initParticleMesh();
  void                  initVariables(int _texSize);
  void                  initFboSettings();
  void                  updateParticlePositions(float _time, float _timeStep);
  void                  updateDistortion(float _time, float _timeStep);
  void                  initDistortForceFbo();
  void                  setupDistortForce();
  void                  setupStartPositions();
  void                  onSliderChange(ofxDatGuiSliderEvent e);
  void                  initPosFbo();
  int                   numParticles;
  int                   textureSize;
  float                 particleOpacity;
  float                 offsetHeight;
  void                  setupStartPositionsFromTextureShape(vector<ofVec3f> startPositions, vector<ofVec3f> startColors);
  ofxDatGuiFolder*      getGUI();
  FboPingPong           particlePosFbo;
  FboPingPong           particleDistortFbo;
  ofTexture             startPosTexture;
  ofVboMesh             particlePoints;
  ofxAutoReloadedShader particlePosShader;
  ofxAutoReloadedShader particleDrawShader;
  ofxAutoReloadedShader particleDistortShader;
  ofFbo::Settings       fboSettings;
  ofxDatGuiFolder*      gui;
  float                 particleSize;
  float                 particleDistance;
  ofImage               particleImage;
  ofImage               map;
  
  ofFbo                 distortFbo;
  ofImage               distortCircle;
  float                 distortFriction;
  float                 cleanSpeed;
  float                 upForce;
  float                 downForce;
  int                   shaderMode;
  
  float                 rotation;
  float                 speedRotation;
  
  float                 maxAge;
  float                 noisePositionScale;
  float                 noiseTimeScale;
  float                 noisePersistence;
  float                 noiseMagnitude;
  ofVec3f               wind;
  
  PositionTexture       startPositions;
};

#endif /* ParticleSystem_hpp */
